#!/bin/perl

#########################################################################################
#											#
#	This perl script take a alleles counts tsv(tab separated) file as		#
#	first argument and convert to sync file.					#
#											#
#	Ex: perl counts2sync.pl file.txt						#
#	output:file.sync								#	#											#
#########################################################################################


use strict;
use warnings;
use File::Basename;

# Get file
my $file = shift;

# Get the directory, file name and extention
my ( $filename, $dir, $ext ) = fileparse( $file, qr{ \. [^.]+ \z }msx );
my $outputName = "$dir" . "$filename" . ".sync";

# Open input file
open my $tsv, $file or die "Could not open $file: $!";

# Create and open output file
open(my $output, '>', "$outputName") or die "Can't create \"$outputName\": $!\n";

# iterate line by line
while( my $line = <$tsv>)  {   
    chomp $line;  
    
    # Skip the header
    if($. == 1) {
	next;
    }
    
    #split line in array  
    my @lineArray = split(/\t/, $line);
    my $str = "";
    my $num = 0;

    # first 3 column of the line (text)
    $str = "Chr1\t" . ($. - 1) . "\tA";
    
    # loop through each allele counts
    for my $i (3..$#lineArray)
    {
	#if allele 1: keep in memory
	if(($i % 2) == 1) {
		$a = $lineArray[$i];

	#if allele 2: change to sync format	
	} else {
		my $b = $lineArray[$i];
		$str = "$str\t" . "$a:$b:0:0:0:0";	
	}	     
    }

    #print final string
    print($output "$str\n");
}


close $output;
close $tsv;
