#!/bin/bash


############################
# 1-Create a base folder where you place the illumina and the key files
# 2-Set the paths and enzyme below
# 3-Run the script: ./uneak.sh
############################


#Paths - To be set
tassel="/home/duceppemo/programs/tassel3.0"
base="/media/D/data/gbs/uneak"
illumina="/media/D/data/gbs/uneak/28pop_33_BC.fq.gz"
key="/media/D/data/gbs/uneak/Keyfile_28pop.txt"
enzyme="PstI-MspI"
#Filters for the last plug-in
MAF="0.01"
mnC="0.08"


#UNEAK command lines
perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -UCreatWorkingDirPlugin -w $base -endPlugin -runfork1 | tee -a $base/uneak_all.log
mv $illumina $base/Illumina
mv $key $base/key
perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -UFastqToTagCountPlugin -w $base -e $enzyme -endPlugin -runfork1 | tee -a $base/uneak_all.log
perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -UMergeTaxaTagCountPlugin -w $base -c 5 -endPlugin -runfork1 | tee -a $base/uneak_all.log
perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -UTagCountToTagPairPlugin -w $base -e 0.03 -endPlugin -runfork1 | tee -a $base/uneak_all.log
perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -UTagPairToTBTPlugin -w $base -endPlugin -runfork1 | tee -a f $base/uneak_all.log
perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -UTBTToMapInfoPlugin -w $base -endPlugin -runfork1 | tee -a $base/uneak_all.log
perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -UMapInfoToHapMapPlugin -w $base -mnMAF $MAF -mxMAF 0.5 -mnC $mnC -mxC 1 -endPlugin -runfork1 | tee -a $base/uneak_all.log


#Log filtering
grep -A 1 "Total number of reads" $base/uneak_all.log > $base/uneak_counts.log
grep -A 2 "Merged tagCount file of all taxa finally contains" $base/uneak_all.log >> $base/uneak_counts.log
grep -A 1 "TagAlignments found" $base/uneak_all.log >> $base/uneak_counts.log
grep -A 1 "HapMap records read" $base/uneak_all.log >> $base/uneak_counts.log
grep -A 1 "tags will be output to ./UNEAK" $base/uneak_all.log >> $base/uneak_counts.log
tr '\n' '@' < $base/uneak_counts.log | sed 's/.cnt@These /#/g' |sed 's/tagCounts\//#/g' | sed 's/ tags were covered by /#/g' | sed 's/ matching reads//g' | sed 's/--@//g' | sed 's/_FC/#FC/g' | tr '#' '\t' | tr '@' '\n' > $base/uneak_tags_reads.log


#hmp, hmc et fas files merging
tr '\n' '@' < $base/hapMap/HapMap.fas.txt | sed 's/@>/#>/g' | tr '#' '\n' | grep "query" | tr '@' '\t' > $base/hapMap/HapMap.fas_query.txt
tr '\n' '@' < $base/hapMap/HapMap.fas.txt | sed 's/@>/#>/g' | tr '#' '\n' | grep "hit" | tr '@' '\t' > $base/hapMap/HapMap.fas_hit.txt
tr '|' '\t' < $base/hapMap/HapMap.hmc.txt > $base/hapMap/HapMap.hmc_mod.txt


#Create a fasta file for each allele
grep query --no-group-separator -A 1 $base/hapMap/HapMap.fas.txt | sed 's/_query_.*//g' > $base/hapMap/HapMap.fas_query.fasta
grep hit --no-group-separator -A 1 $base/hapMap/HapMap.fas.txt | sed 's/_hit_.*//g' > $base/hapMap/HapMap.fas_hit.fasta


#Batch convert all the count (.cnt) files to text (.txt) files. Needed to compute the real allele frequencies.
for f in $(find $base/tagCounts -type f); do
  echo "Processing $f ..."
  perl $tassel/run_pipeline.pl -Xms512m -Xmx48g -fork1 -BinaryToTextPlugin -i $f -o $f.txt -t TagCounts -endPlugin -runfork1
done

