# uneak.sh #

A bash script to run the Universal Network Enabled Analysis Kit (UNEAK) pipeline in TASSEL (http://www.maizegenetics.net/#!tassel/c17q9) and to generate the required file to retrieve exact counts.

##Description##

This script has 5 sections:

1. Running UNEAK.
2. Filtering the log file to extract the most significant numbers.
3. Merging the .hmp, .hmc et .fas files for easy analysis.
4. Creating a fasta file for each allele for every SNP identified.
5. Converting the .cnt file to .txt to retrive the real counts.

##Instructions##

Follow the instructions at the beginning of the script:

1. Create a base folder where you place the illumina and the key files.
2. Set the paths and enzyme below.
3. Run the script: ./uneak.sh.

##Notes##

The files generated at step 4 and 5 are required to extract the real count numbers for the SNP identified by UNEAK (which only reports counts up to 127). Use countExtractor, countMerger and countCleaner under Windows environment to do so.


# countExtractor, countMerger and countCleaner #

* These three programs are meant to be run one after the other.
* countExtractor can take several hours to run.
* countCleaner has versatile filtering capabilities, with some also found in UNEAK's last plugin (mnC and MAF).
* For these reasons, if you are planing to get the exact counts for different mnC and MAF values, it is advised to run countExtractor on the less stringent filters values from UNEAK that you are planing to use. All the SNPs obtained from the more stringent filter values are contained within the ones obtained with the more liberal filter values.


##countExtractor##

* This program runs under Windows environment.
* It is meant to extract the real counts for each allele of every SNPs identified by UNEAK, which is only counting up to 127.
* For each SNP and for each allele, it will retrieve the exact read count for each population.
* It keeps the "TP" numbers and the sequence information for each allele.

###Input###

1. Allele1 (hit) fasta file produced by the uneak.sh script.
2. Allele2 (query) fasta file produced by the uneak.sh script.
3. Count folder containing only the .txt files produced by the uneak.sh script. It's important that only the .txt files are present.
4. Empty allele1 folder.
5. Empty allele2 folder.

###Output###

1. Populated allele1 folder.
2. Populated allele2 folder.


##countMerger##

* This program runs under Windows environment.
* It merges all the counts for all the SNPs for all the populations in a single table separated file.

###Input###

1. Populated allele1 folder (output of countExtractor).
2. Populated allele2 folder (output of countExtractor).
3. Output file (merged counts).

###Output###

1. A single file where all the counts for all the SNPs for all the populations.


##countCleaner##

* This program runs under Windows environment.
* It allows to filter the SNPs according to:

1. Minimum Allele frequency (MAF).
2. Minimum Call rate (mnC).
3. Minimum coverage (minCov).
4. Maximum coverage (maxCov). 

* In UNEAK, the mnC denotes a proportion that how many taxa are covered by at least one tag. countCleaner brings more flexibility to the mnC filter by allowing the user to modify the minimum number tags required and the maximum number of tags allowed. So the mnC, the minCov and the maxCov all work together. For each SNP, when a population does not meet the min or max read count, it is first considered as a missing data. When the proportion of missing data gets higher than the threshold (1-mnC), the SNP is discarded.
* MAF filtering is also available with more precision than the one used for UNEAK, since the later does not use exact count (max 127) and seems to round the frequencies.


###Input###

1. coutMerger output file

###output###

1. Filtered count file at the same location as the input file, with the parameters used added to the file name.

# counts2freq.pl #

* Input: output file from countCleaner.
* Output: file with allele frequencies instead of counts.

# counts2sync.pl #

* Input: output file from countCleaner.
* Output: a sync file ready for analysis by PoPoolation2 to compute Fst values between populations.

# rc2counts.pl #

* input: the _rc file output from PoPoolation2 snp-frequency-diff.pl plugin.
* output: a count file ready to be filtered out with countCleaner.