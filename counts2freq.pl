#!/bin/perl

#########################################################################################
#											#
#	This perl script take a alleles counts tsv(tab separated) file as		#
#	first argument and compute the allelic frequencies.				#
#											#
#	Ex: perl counts2freq.pl file.txt						#
#	output: file_freq.txt								#
#											#
#########################################################################################


use strict;
use warnings;
use File::Basename;

# Get file
my $file = shift;

# Get the directory, file name and extention
my ( $filename, $dir, $ext ) = fileparse( $file, qr{ \. [^.]+ \z }msx );
my $outputName = "$dir" . "$filename" . "_freq$ext";

# Open input file
open my $tsv, $file or die "Could not open $file: $!";

# Create and open output file
open(my $output, '>', "$outputName") or die "Can't create \"$outputName\": $!\n";

# iterate line by line
while( my $line = <$tsv>)  {   
    chomp $line;  
    
    #split line in array  
    my @lineArray = split(/\t/, $line);
    my $str = "";
    #if header
    if($. == 1){
	    # first 3 column of the line 
	    $str = "$lineArray[0]\t$lineArray[1]\t$lineArray[2]";
	    
	    # loop through each allele counts
	    for my $i (3..$#lineArray)
	    {
		#if allele 1: print header column
		if(($i % 2) == 1) {
			$str = "$str\t" . "$lineArray[$i]";
	
		#if allele 2: skip header column	
		} else {
			next;	
		}	     
	    }	
    } else {

	    my $num = 0;
	    # first 3 column of the line (text)
	    $str = "$lineArray[0]\t$lineArray[1]\t$lineArray[2]";
	    
	    # loop through each allele counts
	    for my $i (3..$#lineArray)
	    {
		#if allele 1: keep in memory
		if(($i % 2) == 1) {
			$num = $lineArray[$i];
	
		#if allele 2: compute allelic frequency and add to the string	
		} else {
			my $denom = ($lineArray[$i] + $num);
			$str = "$str\t" . ($num / $denom);	
		}	     
	    }
    }
    #print final string
    print($output "$str\n");
}


close $output;
close $tsv;
