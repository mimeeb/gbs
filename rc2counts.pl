#!/bin/perl

#########################################################################################
#											#
#	This perl script takes PoPoolation2 rc file as					#
#	first argument and makes the format compatible for countCleaner.		#
#											#
#	Ex: perl counts2freq.pl file.txt						#
#	output: file_cnt.txt								#
#											#
#########################################################################################


use strict;
use warnings;
use diagnostics;
use File::Basename;

# Get file
my $file = shift;

# Get the directory, file name and extention
my ( $filename, $dir, $ext ) = fileparse( $file, qr{ \. [^.]+ \z }msx );
my $outputName = "$dir" . "$filename" . "_cnt$ext";

# Open input file
open my $tsv, $file or die "Could not open $file: $!";

# Create and open output file
open(my $output, '>', "$outputName") or die "Can't create \"$outputName\": $!\n";

# iterate line by line
while( my $line = <$tsv>)
{   
	chomp $line;  
	
	#split line in array  
	my @lineArray = split(/\t/, $line);
	my $size = scalar @lineArray;
	my $str = "";
	#if header
	if($. == 1)
	{
		# first 3 column of the line 
		#$str = "$lineArray[1]\t$lineArray[7]\t$lineArray[8]";
		$str = "$lineArray[1]\tMajor_allele\tMinor_allele";
		
		# loop through each allele counts
		for (my $i = 9; $i < (($size-9)/2)+9; $i++)
		{
			$str = "$str\t" . "$lineArray[$i]\t" . "$lineArray[$i]";	 
		}	
	}
	else
	{
		my $num = 0;
		# first 3 column of the line (text)
		my @alleleStates = split("/", $lineArray[4]);
		$str = "$lineArray[1]\t$alleleStates[0]\t$alleleStates[1]";
		
		# loop through each allele counts
		for (my $i = 9; $i < (($size-9)/2)+9; $i++)
		{
			my @num = split("/", $lineArray[$i]);
			my $mja = $num[1]-$num[0];
			$str = "$str\t" . "$num[0]\t" . $mja;		 
		}
	}
	#print final string
	print($output "$str\n");
}


close $output;
close $tsv;
